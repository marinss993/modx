jQuery(function($){
	function iconfiled(listTypeOne){
		switch (listTypeOne) {
			case "text":
			  var adClas = "icon_webfactor-text";
			  break
			case "textarea":
			  var adClas = "icon_webfactor-textarea";
			  break
			case "telephone":
			  var adClas = "icon_webfactor-telephone";
			  break
			case "email":
			  var adClas = "icon_webfactor-mail";
			  break
			case "select":
			  var adClas = "icon_webfactor-dropdownlist";
			  break
			case "radio":
			  var adClas = "icon_webfactor-radio";
			  break
			case "checkbox":
			  var adClas = "icon_webfactor-checkbox";
			  break
			case "color":
			  var adClas = "icon_webfactor-color";
			  break
			case "hidden":
			  var adClas = "icon_webfactor-hidden";
			  break
			case "separate":
			  var adClas = "icon_webfactor-separate";
			  break
			case "htmltagstart":
			  var adClas = "icon_webfactor-startblock";
			  break
			case "htmltagfinish":
			  var adClas = "icon_webfactor-finishblock";
			  break
			default:
			  var adClas = "icon_webfactor-text";
			  break
			}
			return adClas;	
	}
	
	function radioValueChanged(){
        radioValue = $(this).val();
        if($(this).is(":checked") && radioValue == "yes"){
            $(this).parent('fieldset').parent('td').removeClass('redRadio');
            $(this).parent('fieldset').parent('td').parent('tr').addClass('onTrRadio');
        } else {
            $(this).parent('fieldset').parent('td').addClass('redRadio');
			$(this).parent('fieldset').parent('td').parent('tr').removeClass('onTrRadio');
        }
	}

	function sortionTR(){
		var oneThisTDInput = $('#jform_params_list_fields_modal tbody tr');
		$.each(oneThisTDInput, function(index, value){
			$(this).find('td:nth-child(6) input').val(index);
		});
	}
	$(window).load(function(){
		/*Скрытие и показ полей, относящихся к PopUp*/
		var popupYesNoLabel = $('#jform_params_popup .active').attr('for');
		var popupYesNoInput = $('#'+popupYesNoLabel).val();
		var textPopupBtn = $('#jform_params_textcallpopup').parents('.control-group');
		var textBeforeField = $('#jform_params_textbeforeCallButton').parents('.control-group');
		if(popupYesNoInput == 0){
			$(textPopupBtn).css('display', 'none');
			$(textBeforeField).css('display', 'none');
		}else{
			$(textPopupBtn).css('display', 'block');
			$(textBeforeField).css('display', 'block');
		}
		$('.popup-yes-no input').click(function(){
			var popupYesNoLabel = $('#jform_params_popup .active').attr('for');
			var popupYesNoInput = $('#'+popupYesNoLabel).val();
			if(popupYesNoInput == 0){
				$(textPopupBtn).hide(300);
				$(textBeforeField).hide(300);
			}else{
				$(textPopupBtn).show(300);
				$(textBeforeField).show(300);
			}
		});
		
		/*Открытие списка полей*/
		$('#jform_params_list_fields_button').live('click',function(){
			
			var oneThis = $('#jform_params_list_fields_modal tbody tr');
			var heightTR = $( "#jform_params_list_fields_table tbody tr td" ).height();
			$( "#jform_params_list_fields_table tbody tr" ).css('height',heightTR);
			
			$.each(oneThis, function(index, value){
				/*var actualType		= $(this).find('td select').val();*/
				var listTypesOne	= $(this).find('td select[id $=-jform_formfields_typefield]').val();
				var parentDiv = $(this)
					/*.children('td:nth-child(3)')
					.children('select');*/
					.children('td:nth-child(4)');
				if(listTypesOne == 'text' || listTypesOne == 'textarea' || listTypesOne == 'color' || listTypesOne == 'hidden' || listTypesOne == 'email' || listTypesOne == 'separate' || listTypesOne == 'htmltagstart' || listTypesOne == 'htmltagfinish'){
					$(this).find('td textarea[id$=jform_formfields_paramsfield]').css('display','none');
				}
				
				$(parentDiv).removeClass('icon_webfactor-mail icon_webfactor-telephone icon_webfactor-text icon_webfactor-checkbox icon_webfactor-dropdownlist icon_webfactor-radio icon_webfactor-separate icon_webfactor-textarea icon_webfactor-color icon_webfactor-hidden icon_webfactor-startblock icon_webfactor-finishblock').addClass(iconfiled(listTypesOne)).attr("data-type-field",listTypesOne);
				
				/*Условие для блокирования полей типа DIV*/
				if(listTypesOne == 'htmltagstart'){
					$(this).find('td input[id^=jform_formfields_namefield]').val("div").prop( "disabled", true );
					$(this).find('td input[id^=jform_formfields_nameforpost]').prop( "disabled", false );
				}else if(listTypesOne == 'htmltagfinish'){
					$(this).find('td input[id^=jform_formfields_namefield]').val("div").prop( "disabled", true );
					$(this).find('td input[id^=jform_formfields_nameforpost]').val("").prop( "disabled", true );
				}
			});
			
			/*Смена типа поля*/
			$('#jform_params_list_fields_modal tbody tr td select[id $=-jform_formfields_typefield]').live('click',function(){
				var parentTR = $(this)
								.parent('td')
								.parent('tr');
				var parentDiv = $(this)
								.parent('td');
				var oneThisSpan = $(this);
				$(parentDiv).addClass('change');
				if(parentDiv.hasClass('change')){
					var actualType		= $(parentDiv).attr("data-type-field");
					var listTypesOne	= $(this).val();
					
					if(listTypesOne == 'text' || listTypesOne == 'textarea' || listTypesOne == 'color' || listTypesOne == 'hidden' || listTypesOne == 'email' || listTypesOne == 'separate' || listTypesOne == 'htmltagstart' || listTypesOne == 'htmltagfinish'){
						$(parentTR).find('td textarea[id$=jform_formfields_paramsfield]').fadeOut(300);
					}else{
						$(parentTR).find('td textarea[id$=jform_formfields_paramsfield]').fadeIn(300);
						if(listTypesOne == 'telephone'){
							$(parentTR).find('td textarea[id$=jform_formfields_paramsfield]').val("+38(099)999-99-99");
						}
						if(listTypesOne == 'select' || listTypesOne == 'radio' || listTypesOne == 'checkbox'){
							$(parentTR).find('td textarea[id$=jform_formfields_paramsfield]').val("params1; params2; params3");
						}
					}
					
					/*Условие для блокирования полей типа DIV*/
					if(listTypesOne == 'htmltagstart'){
						$(parentTR).find('td input[id$=jform_formfields_namefield]').val("div").prop( "disabled", true );
						$(parentTR).find('td input[id$=jform_formfields_nameforpost]').prop( "disabled", false );
					}else if(listTypesOne == 'htmltagfinish'){
						$(parentTR).find('td input[id$=jform_formfields_namefield]').val("div").prop( "disabled", true );
						$(parentTR).find('td input[id$=jform_formfields_nameforpost]').val("").prop( "disabled", true );
					}else{
						/*$(parentTR).find('td input[id^=jform_formfields_namefield]').val("").prop( "disabled", false );
						$(parentTR).find('td input[id^=jform_formfields_nameforpost]').val("").prop( "disabled", false );*/
					}
					
					$(parentDiv).removeClass('icon_webfactor-mail icon_webfactor-telephone icon_webfactor-text icon_webfactor-checkbox icon_webfactor-dropdownlist icon_webfactor-radio icon_webfactor-separate icon_webfactor-textarea icon_webfactor-color icon_webfactor-hidden icon_webfactor-startblock icon_webfactor-finishblock').addClass(iconfiled(listTypesOne)).attr('data-type-field',listTypesOne);
				}
			});
		});
		
		/*Сортировка*/
		$( "#jform_params_list_fields_table tbody" ).sortable({
			stop: sortionTR
		});
		
		/*Подсветка радио кнопок*/
		$('#jform_params_list_fields_modal tr td fieldset input[name$=onoff]').live('change',radioValueChanged);
		
		setInterval(function(){
			var RVCh = $("#jform_params_list_fields_modal tr td fieldset input[name$=onoff]");
			
			$(RVCh).each(function(){
				if($(this).is(':checked') && $(this).val() == "no"){
					$(this).parent('fieldset').parent('td').addClass('redRadio');
					$(this).parent('fieldset').parent('td').parent('tr').addClass('desabled-field');
					$(this).parent('fieldset').parent('td').parent('tr').removeClass('onTrRadio');
				}
				
				if($(this).is(':checked') && $(this).val() == "yes"){
					$(this).parent('fieldset').parent('td').removeClass('redRadio');
					$(this).parent('fieldset').parent('td').parent('tr').removeClass('desabled-field');
					$(this).parent('fieldset').parent('td').parent('tr').addClass('onTrRadio');
				}
			});
		}, 1000);

		/*Добавление нового поля*/
		$('#jform_params_list_fields_modal tr th:last-child div a.button').live('click', function(){
			sortionTR
			/*var actualType		= $(this)
									.parent('div')
									.parent('th')
									.parent('tr')
									.parent('thead')
									.parent('table')
									.find('td select[id $=-jform_formfields_typefield]')
									.val();*/
			var listTypesOne	= $(this)
									.parent('div')
									.parent('th')
									.parent('tr')
									.parent('thead')
									.parent('table')
									.find('td select[id $=-jform_formfields_typefield]')
									.val();
			var parentDiv = $(this)
								.parent('div')
								.parent('th')
								.parent('tr')
								.parent('thead')
								.parent('table')
								.children('tbody')
								.children('tr:last-child')
								.children('td:nth-child(4)');
			if(listTypesOne == 'text' || listTypesOne == 'textarea' || listTypesOne == 'color' || listTypesOne == 'hidden' || listTypesOne == 'email' || listTypesOne == 'separate' || listTypesOne == 'htmltagstart' || listTypesOne == 'htmltagfinish'){
				$(this).parent('div').parent('th').parent('tr').parent('thead').parent('table').find('tr:last-child td textarea[id$=jform_formfields_paramsfield]').css('display','none');
			}
			$(parentDiv).removeClass('icon_webfactor-mail icon_webfactor-telephone icon_webfactor-text icon_webfactor-checkbox icon_webfactor-dropdownlist icon_webfactor-radio icon_webfactor-separate icon_webfactor-textarea icon_webfactor-color icon_webfactor-hidden icon_webfactor-startblock icon_webfactor-finishblock').addClass('icon_webfactor-text').attr('data-type-field','text');
		});

		/*$( "#jform_params_list_fields_table tbody" ).disableSelection();*/
		
		/*Отслеживание наличия парных блоков НЕ ГОТОВО!!!*/
		setInterval(function(){
			
			var countStart = 0;
			var countFinish = 0;
			var countStartOn = 0;
			var countFinishOn = 0;
			
			var countStart = $("select[id$=jform_formfields_typefield] option[value=htmltagstart]:selected").length;
			var countFinish = $("select[id$=jform_formfields_typefield] option[value=htmltagfinish]:selected").length;
			
			var countStartOn = $("tr.onTrRadio select[id$=jform_formfields_typefield] option[value=htmltagstart]:selected").length;
			var countFinishOn = $("tr.onTrRadio select[id$=jform_formfields_typefield] option[value=htmltagfinish]:selected").length;


			/*Прячем или показываем кнопку SAVE в зависимости от соблюдения условия парности полей типа DIV*/
			if(countStartOn != countFinishOn){
				if(countStartOn || countFinishOn){
					/*alert("Внимание! В форме есть не закрытые теги блоков!");*/
					if(!$("div").is("#undabledBlocks")){
						$("#jform_params_list_fields_modal .modal-footer").prepend('<div id="undabledBlocks"></div>');
					}
					$("#jform_params_list_fields_modal .modal-footer button.save-modal-data").fadeOut();
					$("#undabledBlocks").html("Имеются не закрытые блоки DIV или они не включены!");
				}
				$("#filedOrderBlocks").remove();
			}else{
				$("#undabledBlocks").remove();
				$("#filedOrderBlocks").remove();
				/*$("#jform_params_list_fields_modal .modal-footer button.save-modal-data").fadeIn();*/
				/*$("#undabledBlocks").html("");*/	
				
				/*Неверный порядок блоков DIV*/
				var lastEl = $("tr.onTrRadio select[id$=jform_formfields_typefield] option[value^=htmltag]:selected").last().val();
				var firstEl = $("tr.onTrRadio select[id$=jform_formfields_typefield] option[value^=htmltag]:selected").first().val();
				if(lastEl && lastEl != 'htmltagfinish'){
					
					/*alert(lastEl);*/
					/*alert("Внимание! В форме есть не закрытые теги блоков!");*/
					if(!$("div").is("#filedOrderBlocks")){
						$("#jform_params_list_fields_modal .modal-footer").prepend('<div id="filedOrderBlocks"></div>');
					}
					$("#jform_params_list_fields_modal .modal-footer button.save-modal-data").fadeOut();
					$("#filedOrderBlocks").html("Неверный порядок. Закрывающий блок находиться до открывающего!");
				}else if(lastEl && lastEl != 'htmltagfinish' &&  firstEl && firstEl != 'htmltagstart'){
					$("#jform_params_list_fields_modal .modal-footer button.save-modal-data").fadeIn();
					$("#filedOrderBlocks").remove();
				}else{
					$("#jform_params_list_fields_modal .modal-footer button.save-modal-data").fadeIn();
				}
			}
		}, 1000);
		
		$('#jform_params_list_fields_modal .save-modal-data').hover(sortionTR);
	});
	$('#module-form .form-inline-header').append('<div class="obraschenie_avtora"><p>Спасибо за то, что выбрали <strong>расширение Ajax WebFactor Form</strong> для решения своих задач.</p><p>Вы всегда можете отблагодарить автора:</p><ul><li>Приват Банк: 5167 9823 0090 0906</li><li>Yandex Деньги: 410011877755805</li><li>PayPall: orsy81@gmail.com</li><li>VK по ссылке: <a href="https://vk.com/webors?w=app5727453_-22201371" target="_blank">поблагодарить</a></li></ul></div>');
});