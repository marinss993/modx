jQuery(document).on( 'scroll', function(){
	scrollFromTop();
});

function scrollFromTop(){
	var fff = window.pageYOffset || document.documentElement.scrollTop;
	return fff;
}

function positionAfterSend(heightblock){
    var heightpage = jQuery(window).height();
	var heightpopupblock 	= jQuery(heightblock).height();
	var scrollFT 			= scrollFromTop();
	var position			= heightpage/2 + heightpopupblock + scrollFT;
	return position;
}

function OpenModalForm(openbutton,modal,overley){
	jQuery(openbutton).click( function(event){ // ловим клик по ссылки с id="go"
		event.preventDefault(); // выключаем стандартную роль элемента
        var heightpage = jQuery(window).height();
		var heightpopupblock = jQuery(modal).height();
		if(heightpopupblock >= heightpage){
			var scrollFT 		= scrollFromTop();
			var positiOnScroll 	= 50 + scrollFT;
			var positiForOK 	= heightpage/2 - heightpopupblock/2 + scrollFT;
		}else{
			var scrollFT 		= scrollFromTop();
			var positiOnScroll 	= heightpage/2 - heightpopupblock/2 + scrollFT;
			var positiForOK 	= heightpage/2 - heightpopupblock/2 + scrollFT;
		}
		
		jQuery(overley).fadeIn(400, // сначала плавно показываем темную подложку
			function(){ // после выполнения предыдущей анимации
				jQuery(modal) 
					.css('display', 'block') // убираем у модального окна display: none;
					.animate({opacity: 1, top: positiOnScroll}, 200); // плавно прибавляем прозрачность одновременно со съезжанием вниз
		});
	});
}

function CloseModalForm(close,overley,modal){
	jQuery(close).click( function(){ // ловим клик по крестику или подложке
		jQuery(modal).animate({top: 0}, 200);
		jQuery(modal)
			.animate({opacity: 0}, 300, function(){ // после анимации
				jQuery(this).css('display', 'none'); // делаем ему display: none;
				jQuery(overley).fadeOut(400); // скрываем подложку
				}
			);
	});
}

function hideBlockFormAfterSend(modal,overley,status,response){
	
	jQuery(status).html(response);
	
    var heightpage = jQuery(window).height();
	var heightpopupblock = jQuery(status).height();
	var scrollFT 		= scrollFromTop();
	var positiOnScroll 	= heightpage/2 - heightpopupblock/2 + scrollFT;
	
	jQuery(modal).animate({top: positiOnScroll}, 400, function(){
		setTimeout(function (){
			jQuery(modal)
			// плавно меняем прозрачность на 0 и одновременно двигаем окно вверх
			.animate({top: -positiOnScroll}, 400,
				function(){ // после анимации
					jQuery(this).fadeOut(); // делаем ему display: none;
					jQuery(overley).fadeOut(); // скрываем подложку
				}
			);	
		}, 3000);		
	});
	

}

function hideAndClearFormAfterSend(modal,overley,status,response,textbutton){
	
	jQuery(status).html(response);
	
    var heightpage = jQuery(window).height();
	var heightpopupblock = jQuery(status).height();
	var scrollFT 		= scrollFromTop();
	var positiOnScroll 	= heightpage/2 - heightpopupblock/2 + scrollFT;
	
	
	jQuery(modal).animate({top: positiOnScroll}, 400, function(){
		setTimeout(function (){
			jQuery(modal)
			// плавно меняем прозрачность на 0 и одновременно двигаем окно вверх
			.animate({top: -positiOnScroll}, 400,
				function(){ // после анимации
					jQuery(this).fadeOut(); // делаем ему display: none;
					jQuery(overley).fadeOut('', function(){ // скрываем подложку
						jQuery(status).fadeOut();			
						jQuery(modal + " form").fadeIn();
						jQuery(modal + " form")[0].reset();
						jQuery(modal + " input[id^=submit]").attr('value', textbutton);
					}); 
					
				}
			);	
		}, 3000);		
	});
}

/*For Static Form*/

function hideBlockStaticFormAfterSend(block,status,response){
	
	jQuery(status).html(response);
	
	jQuery(block + " form").fadeOut(400, function(){
		jQuery(status).fadeIn(400);
	}); // делаем ему display: none;
	
}

function hideAndClearStaticFormAfterSend(block,status,response,textbutton){
	
	jQuery(status).html(response);
	
	jQuery(block + " form").fadeOut(400, function(){
		jQuery(status).fadeIn(400, function(){
			setTimeout(function (){
				jQuery(status).fadeOut(400, function(){
					jQuery(block + " form").fadeIn(400, function(){
						jQuery(block + " form")[0].reset();
						jQuery(block + " input[id^=submit]").attr('value', textbutton);
					}); 
				});
			}, 3000);
		});
	});
}