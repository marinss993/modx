<?php defined('_JEXEC') or die;

// Include the helper.
require_once __DIR__ . '/helper.php';
require_once __DIR__ . '/includes/buildfield.php';
$moduleclass_sfx = htmlspecialchars( $params->get( 'moduleclass_sfx' ) );

$onoffpopup				= $params->get( 'popup' );
$nameInOut				= $params->get( 'nameInOut' );
$onoffjquery			= $params->get( 'onoffjquery' );
$textbeforeCallButton	= $params->get( 'textbeforeCallButton' );
$textbeforeform			= $params->get( 'textbeforeform' );
$textcallpopup			= $params->get( 'textcallpopup' );

$sendtoemail 			= $params->get( 'sendtoemail' );
$sendtoemailcc 			= $params->get( 'sendtoemailcc' );
$sendtoemailbcc 		= $params->get( 'sendtoemailbcc' );

$sendfromemail 			= $params->get( 'sendfromemail' );

$clearaftersend			= $params->get( 'clearaftersend' );

$subjectofmail 			= $params->get( 'subjectofmail' );
$textsuccesssend 		= $params->get( 'textsuccesssend' );
$textsubmit 			= $params->get( 'textsubmit' );
$textwhensending 		= $params->get( 'textwhensending' );
$jsbeforesend			= $params->get( 'jsbeforesend' );
$spinecode				= $params->get( 'spinecode' );
$colorscheme 			= $params->get( 'colorscheme' );

$allparams				= json_decode($params->get( 'list_fields' ));
$moduleid 				= $module->id;
$moduleTitle			= $module->title;
$field					= modAjaxWebfactorFormBuildfield::buildFields($allparams, $moduleid, $onoffpopup, $nameInOut);
//$dataField			= modAjaxWebfactorFormBuildfield::dataFields($allparams, $moduleid);
$dataField				= modAjaxWebfactorFormBuildfield::getFieldsData($allparams, $moduleid);
$ajaxRequest			= modAjaxWebfactorFormBuildfield::ajaxRequestFields($allparams, $moduleid);
//$ajaxDataFields		= modAjaxWebfactorFormBuildfield::ajaxDataField($allparams, $moduleid);
//$validateFieldsForm	= modAjaxWebfactorFormBuildfield::validateFieldsForm($allparams, $moduleid);

// Instantiate global document object
$doc = JFactory::getDocument();

//$doc->addScriptDeclaration($js);

require JModuleHelper::getLayoutPath('mod_ajax_webfactor_form', $params->get('layout', 'default'));