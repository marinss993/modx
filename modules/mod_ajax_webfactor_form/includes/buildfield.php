<?php defined('_JEXEC') or die;

class modAjaxWebfactorFormBuildfield{
	//Формируем поля
	public static function buildFields($allparams, $moduleid, $onoffpopup, $nameInOut){//формируем поля для вывода на странице
		$fieldbuiding = array();
		$poryadokF = 0;
		for($i = 0; $i < count($allparams->namefield); $i++){
			
			$namefield 		= $allparams->namefield[$i];
			$nameforpost 	= $allparams->nameforpost[$i];
			$typefield 		= $allparams->typefield[$i];
			$paramsfield 	= $allparams->paramsfield[$i];
			$sortnumber 	= $allparams->sortnumber[$i];
			$onoff 			= $allparams->onoff[$i];
			$required		= $allparams->required[$i];
			if($required == "yes"){
				$requiredField = "required ";
				$reqstar = "*";
			}else{
				$requiredField = "";
				$reqstar = "";
			}
			
			$nameforfield 	= $typefield.$i.$moduleid;
			
			if($onoff == "yes"):
			
				switch($typefield){
					case "hidden":
						$fieldB = "<input id=\"".$nameforfield."\" class=\"hidden\" type=\"hidden\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" placeholder=\"".$namefield."\">";
					break;
					
					case "text":
						if($nameInOut){
							$fieldB = "<input id=\"".$nameforfield."\" class=\"text\" type=\"text\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" placeholder=\"".$namefield.$reqstar."\">";
						}else{
							$fieldB = "<div class=\"WFnameField\"><p>".$namefield.$reqstar."</p>";
							$fieldB .= "<input id=\"".$nameforfield."\" class=\"text\" type=\"text\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" >";
							$fieldB .= "</div>";
						}
					break;
					
					case "textarea":
						if($nameInOut){
							$fieldB = "<textarea id=\"".$nameforfield."\" type=\"textarea\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" placeholder=\"".$namefield.$reqstar."\" rows=\"5\" cols=\"45\"></textarea>";
						}else{
							$fieldB = "<div class=\"WFnameField\"><p>".$namefield.$reqstar."</p>";
							$fieldB .= "<textarea id=\"".$nameforfield."\" type=\"textarea\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" rows=\"5\" cols=\"45\"></textarea>";
							$fieldB .= "</div>";
						}
					break;
					
					case "telephone":
						if($nameInOut){
							$fieldB = "<input id=\"".$nameforfield."\" data-allready=\"0\" class=\"webfactor-telephone\" type=\"tel\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" placeholder=\"".$namefield.$reqstar."\">";
							$fieldB .= "<script>jQuery(function($){";
							//$fieldB .= "$(\"#".$nameforfield."\").mask(\"+38(099)999-99-99\");});";
							if(!empty($paramsfield)){
								$fieldB .= "$(\"#".$nameforfield."\").inputmask({";
								$fieldB .= 	"\"mask\": \"".$paramsfield."\",";
								$fieldB .= 	"\"oncomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"1\"); }," ;
								$fieldB .= 	"\"onincomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"0\"); },";
								$fieldB .= "});";
							}else{
								$fieldB .= "$(\"#".$nameforfield."\").inputmask({";
								$fieldB .= 	"\"mask\": \"+38(099)999-99-99\",";
								$fieldB .= 	"\"oncomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"1\"); },";
								$fieldB .= 	"\"onincomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"0\"); },";
								$fieldB .= "});";
							}
							
							$fieldB .= "});";
							if($onoffpopup){
								$fieldB .= "<\/script>";
							}else{
								$fieldB .= "</script>";
							}
						}else{
							$fieldB = "<div class=\"WFnameField\"><p>".$namefield.$reqstar."</p>";
							$fieldB .= "<input id=\"".$nameforfield."\" class=\"webfactor-telephone\" type=\"text\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" >";
							$fieldB .= "<script>jQuery(function($){";
							//$fieldB .= "$(\"#".$nameforfield."\").mask(\"+38(099)999-99-99\");});";
							if(!empty($paramsfield)){
								$fieldB .= "$(\"#".$nameforfield."\").inputmask({";
								$fieldB .= 	"\"mask\": \"".$paramsfield."\",";
								$fieldB .= 	"\"oncomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"1\"); }," ;
								$fieldB .= 	"\"onincomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"0\"); },";
								$fieldB .= "});";
							}else{
								$fieldB .= "$(\"#".$nameforfield."\").inputmask({";
								$fieldB .= 	"\"mask\": \"+38(099)999-99-99\",";
								$fieldB .= 	"\"oncomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"1\"); },";
								$fieldB .= 	"\"onincomplete\": function(){ $(\"#".$nameforfield."\").attr(\"data-allready\", \"0\"); },";
								$fieldB .= "});";
							}
                            
							$fieldB .= "});";
							if($onoffpopup){
								$fieldB .= "<\/script>";
							}else{
								$fieldB .= "</script>";
							}
							$fieldB .= "</div>";
						}
					
					break;
					
					case "email":
						if($nameInOut){
							$fieldB = "<input id=\"".$nameforfield."\" class=\"webfactor-email\" type=\"email\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" placeholder=\"".$namefield.$reqstar."\">";
						}else{
							$fieldB = "<div class=\"WFnameField\"><p>".$namefield.$reqstar."</p>";
							$fieldB .= "<input id=\"".$nameforfield."\" class=\"webfactor-email\" type=\"email\" ".$requiredField."name=\"".$nameforfield."\" value=\"\" >";
							$fieldB .= "</div>";
						}
					break;
					
					case "select":
						$selects = explode(";", $paramsfield);
						$fieldB = "<div class=\"select\"><select id=\"".$nameforfield."\" name=\"".$nameforfield."\" ".$requiredField.">";
						$fieldB .= "<option disabled selected>".$namefield.$reqstar."</option>";
						for($y = 0; $y < count($selects); $y++){
							$fieldB .= "<option value=\"".trim($selects[$y])."\">".$selects[$y]."</option>";
						}
						$fieldB .= "</select></div>";
					break;
					
					case "radio":
						$radios = explode(";", $paramsfield);
						$fieldB = "<fieldset id=\"".$nameforfield."\" class=\"radio\"><legend>".$namefield.$reqstar."</legend>";
						for($y = 0; $y < count($radios); $y++){
							$fieldB .= "<p>";
							$fieldB .= "<input id=\"".$nameforfield.$y."\" type=\"radio\" ".$requiredField."name=\"".$nameforfield."\" value=\"".trim($radios[$y])."\">";
							$fieldB .= "<label for=\"".$nameforfield.$y."\">";
							$fieldB .= trim($radios[$y]);
							$fieldB .= "</label>";
							$fieldB .= "</p>";
							$requiredField = "";
						}
						$fieldB .= "<br></fieldset>";
					break;
					
					case "checkbox":
						$checkboxes = explode(";", $paramsfield);
						$fieldB = "<fieldset for=\"".$nameforfield."\" class=\"checkbox\"><legend>".$namefield.$reqstar."</legend>";
						for($y = 0; $y < count($checkboxes); $y++){
							$fieldB .= "<p><label for=\"".$nameforfield.$y."\"><input id=\"".$nameforfield.$y."\" type=\"checkbox\"  ".$requiredField."name=\"".$nameforfield."\" value=\"".trim($checkboxes[$y])."\"> ".trim($checkboxes[$y])."</label></p>";
							$requiredField = "";
						}
						$fieldB .= "<br></fieldset>";
					break;
					
					case "color":
						$fieldB = "<div class=\"colorbox\"><p>".$namefield.$reqstar."</p>";
						$fieldB .= "<input id=\"".$nameforfield."\" type=\"color\" value=\"#424242\" name=\"".$nameforfield."\"></div>";
					break;
					
					case "separate":
						$fieldB = "<hr id=\"".$namefield."\" class=\"separate\" />";
					break;
					
					case "htmltagstart":
						if(!$namefield){
							$fieldB = "<".$namefield." class=\"".$nameforpost."\">";
						}else{
							$fieldB = "<div class=\"".$nameforpost."\">";
						}
					break;
					
					case "htmltagfinish":
						if(!$namefield){
							$fieldB = "</".$namefield.">";
						}else{
							$fieldB = "</div>";
						}
					break;
				}
			$fieldbuiding[$poryadokF] = array("sort"=>$sortnumber, "dataField"=>$fieldB);
			
			$poryadokF++;
			endif;
		}
		
		foreach ($fieldbuiding as $key => $row) {
				$sort[$key]  = $row['sort'];
				$dataField[$key] = $row['dataField'];
			}
		array_multisort($sort, SORT_NUMERIC, $dataField, SORT_STRING, $fieldbuiding);
		
		return $fieldbuiding;
	}
	// формируем код для получения данных из формы
	public static function getFieldsData($allparams, $moduleid){ 
		$fieldGetVal = "";	
		for($i = 0; $i < count($allparams->namefield); $i++){
			
			$namefield 		= $allparams->namefield[$i];
			$typefield 		= $allparams->typefield[$i];
			$paramsfield 	= $allparams->paramsfield[$i];
			$sortnumber 	= $allparams->sortnumber[$i];
			$onoff 			= $allparams->onoff[$i];
			
			$nameforfield 	= $typefield.$i.$moduleid;
			
			if($onoff == "yes" && $typefield != "separate" && $typefield != "htmltagstart" && $typefield != "htmltagfinish"):
			switch($typefield){
				case "hidden":
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]').val();\n";
				break;
				
				case "text":
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]').val();\n";
				break;
				
				case "textarea":
				$fieldGet = "var ".$nameforfield." = $('textarea[name=".$nameforfield."]').val();\n";
				break;
				
				case "email":
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]').val();\n";
				break;
				
				case "telephone":
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]').val();\n";
				break;
				
				case "select":
				$fieldGet = "var ".$nameforfield." = $('select[name=".$nameforfield."] option:selected').val();\n";
				break;
				
				case "radio":
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]:checked').val();\n";
				break;
				
				case "checkbox":				
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]:checked').map( function() {\n";
				$fieldGet .= "return this.value;\n";
				$fieldGet .= "}).get().join(',');\n";
				break;
				
				case "color":
				$fieldGet = "var ".$nameforfield." = $('input[name=".$nameforfield."]').val();\n";
				break;
			}
			$fieldGetVal .= $fieldGet;
			endif;
		}
		return $fieldGetVal;
	}
	//формируем код для отправки данный формы AJAX-ом	
	public static function ajaxRequestFields($allparams, $moduleid){
		$ajaxData = "";
		for($i = 0; $i < count($allparams->namefield); $i++){
			
			//$namefield 		= $allparams->namefield[$i];
			$typefield 		= $allparams->typefield[$i];
			$onoff 			= $allparams->onoff[$i];
			$nameforfield 	= $typefield.$i.$moduleid;
			
			if($onoff == "yes" && $typefield != "separate" && $typefield != "htmltagstart" && $typefield != "htmltagfinish"):			
			$ajaxData	.= "'".$nameforfield."' : ".$nameforfield.",\n";
			endif;
			
		}
		return $ajaxData;
	}
	
	public static function ajaxDataField($allparams, $moduleid){
		$ajaxDataFields = array();
		for($i = 0; $i < count($allparams->namefield); $i++){
			
			//$namefield 		= $allparams->namefield[$i];
			$typefield 		= $allparams->typefield[$i];
			$onoff 			= $allparams->onoff[$i];
			$nameforpost	= $allparams->nameforpost[$i];
			$nameforfield 	= $typefield.$i.$moduleid;
			
			if($onoff == "yes" && $typefield != "separate" && $typefield != "htmltagstart" && $typefield != "htmltagfinish"):
				$ajaxDataFields[]	= array("nameforfield"=>$nameforfield, "nameforpost"=>$nameforpost);
			endif;
		}
		return $ajaxDataFields;
	}
	
	public static function validateFieldsForm($allparams, $moduleid){
		$validateFieldName = "";
		$validateRules = "";
		for($i = 0; $i < count($allparams->namefield); $i++){
			//$namefield 		= $allparams->namefield[$i];
			$typefield 		= $allparams->typefield[$i];
			$onoff 			= $allparams->onoff[$i];
			$required		= $allparams->required[$i];
			$nameforfield 	= $typefield.$i.$moduleid;
			if($onoff == "yes" and $required == "yes"):
				$validateFieldName[] = $nameforfield;
			endif;
		}
		$validateRules .= "if(";
		for($y = 0; $y < count($validateFieldName); $y++){
			if($y != count($validateFieldName)-1){
				$validateRules .= $validateFieldName[$y]." == '' && ";
			}else{
				$validateRules .= $validateFieldName[$y]." == ''";
			}
		}
		$validateRules .= "){alert('Не все поля заполнены');}";
		return $validateRules;
	}
}
