<?php defined('_JEXEC') or die;

class modAjaxWebfactorFormHelper{
	public static function getAjax(){
		$configure			= JFactory::getConfig();
		$sitenameForForm	= $configure->get('sitename');
		$mailfromForForm	= $configure->get('mailfrom');
		
		 // Принимаем значения полей которые нам отправил Ajax
		$input					= JFactory::getApplication()->input;
		//print_r($input);
		
		//$moduleid 				= $module->id;
		$moduleid 				= $input->get('data-modid','','STRING');
		$moduleTitle			= $input->get('modtitle','','STRING');
		
		$currentPage			= $input->get('currentPage','','STRING');
		
		jimport('joomla.application.module.helper');
		$module					= JModuleHelper::getModule('ajax_webfactor_form', $moduleTitle);
		$params					= new JRegistry();
		$params->loadString($module->params);
		
		//$allFieldsParams		= json_decode($params->get( 'list_fields' ));
		$allparams				= json_decode($params->get( 'list_fields' ));
		
		
		
		$sendtoemailAjax		= $params->get( 'sendtoemail' );
		$sendtoemailAjaxCC		= $params->get( 'sendtoemailcc' );
		$sendtoemailAjaxBCC		= $params->get( 'sendtoemailbcc' );
        
        if($params->get( 'textbeforemassage' )){
            $textbeforemassage        = $params->get( 'textbeforemassage' ) . ",\n";
        }else{
            $textbeforemassage = "";
        }
		
		if($params->get( 'sendfromemail' )){
			$sendfromemailAjax 		= $params->get( 'sendfromemail' );
		}else{
			$sendfromemailAjax 		= $mailfromForForm;
		}
		
		if($params->get( 'sendfromname' )){
			$sendfromenameAjax 		= $params->get( 'sendfromname' );
		}else{
			$sendfromenameAjax 		= $sitenameForForm;
		}
		
		if($params->get( 'subjectofmail' )){
			$subjectofmailAjax 		= $params->get( 'subjectofmail' );
		}else{
			$subjectofmailAjax 		= "Сообщение с сайта " . $sitenameForForm;
		}
		
		
		$textsuccesssendAjax	= $params->get( 'textsuccesssend' );
		$textsubmitAjax 		= $params->get( 'textsubmit' );
		
		
		//$bodymail	 = '<table cellpadding="10">'.$textsuccesssendAjax;
        if($textbeforemassage){
            $bodymail   = $textbeforemassage;
            $bodymail	.= '<table cellpadding="10">';
        }else{
            $bodymail	 = '<table cellpadding="10">';
        }
		$ajaxDataFields = modAjaxWebfactorFormBuildfield::ajaxDataField($allparams, $moduleid);
		$replyToEmail = "";
		$replyToName = "";
		for($i = 0; $i < count($ajaxDataFields); $i++){
			$bodymail .= "<tr>";
			$bodymail .= "<td>".$ajaxDataFields[$i]["nameforpost"]."</td>";
			$bodymail .= "<td>".$input->get($ajaxDataFields[$i]["nameforfield"],'','STRING')."</td>";
			$bodymail .= "</tr>";
			if($ajaxDataFields[$i]["nameforpost"] == "E-mail" || $ajaxDataFields[$i]["nameforpost"] == "e-mail" || $ajaxDataFields[$i]["nameforpost"] == "email" || $ajaxDataFields[$i]["nameforpost"] == "Email"){
				$replyToEmail = $input->get($ajaxDataFields[$i]["nameforfield"],'','STRING');
			}
			if($ajaxDataFields[$i]["nameforpost"] == "Имя" || $ajaxDataFields[$i]["nameforpost"] == "имя" || $ajaxDataFields[$i]["nameforpost"] == "Name" || $ajaxDataFields[$i]["nameforpost"] == "name"){
				$replyToName = $input->get($ajaxDataFields[$i]["nameforfield"],'','STRING');
			}
		}
		$bodymail	.= '</table>';
		
		$bodymail	.= '<p>Страница отправки формы: <a href="'. $currentPage .'">' . $currentPage . '</a></p>';
		
		// Отправка email
		
		//Получаем экземпляр класса JMail
		$mailer = JFactory::getMailer();
		//Указываем что письмо будет в формате HTML
		$mailer->IsHTML( true );
		//Указываем отправителя письма
		$mailer->setSender( array( $sendfromemailAjax, $sendfromenameAjax ) );
		//указываем получателя письма
		//$mailer->addRecipient( array($sendtoemailAjax));
		$mailer->addRecipient( $sendtoemailAjax );
		//добавляем получателя копии
		$mailer->addCc( $sendtoemailAjaxCC );
		//добавляем получателя копии
		$mailer->addBcc( $sendtoemailAjaxBCC );
		
		//добавляем адрес для ответа
		if($replyToEmail != ""){
			if($replyToName == ""){
				$replyToName = "Уважаемый незнакомец";
			}
			$mailer->addReplyTo($replyToEmail, $replyToName);
		}
		//добавляем вложение
		//$mailer->addAttachment( '' );
		//Добавляем Тему письма
		$mailer->setSubject($subjectofmailAjax);
		//Добавляем текст письма
		$mailer->setBody($bodymail);
		//Отправляем письмо
		$mailer->send();
	
		return $textsuccesssendAjax; // Ответ Ajax'у
	}
}