<?php defined('_JEXEC') or die;

$document			= JFactory::getDocument();
//$configure			= JFactory::getConfig();

JHTML::stylesheet('modules/' . $module->module . '/css/color-scheme/'.$colorscheme.'/style.css');
//JHTML::script('jquery.form.js', 'modules/mod_ajax_webfactor_form/js/');
JHtml::_('jquery.framework');

if($onoffjquery){
	$document->addScript("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js");
}
$document->addScript("modules/mod_ajax_webfactor_form/js/jquery.validate.min.js");
$document->addScript("modules/mod_ajax_webfactor_form/js/jquery.form.js");
//$document->addScript("modules/mod_ajax_webfactor_form/js/jquery.maskedinput.js");
include_once "modules/mod_ajax_webfactor_form/js/messages_ru.min.js.php";

//$document->addScript("modules/mod_ajax_webfactor_form/js/inputmask.js");
$document->addScript("modules/mod_ajax_webfactor_form/js/jquery.inputmask.bundle.min.js");

$document->addScriptDeclaration( $scriptValidate );
$document->addScript("modules/mod_ajax_webfactor_form/js/custom.js");
//$document->addScript("modules/mod_ajax_webfactor_form/js/typed.js");
//$webfactordoc->addStyleSheet($this->baseurl."/modules/mod_ajax_webfactor_form/style/default/webfactor.css");

if($onoffpopup){
	$formrender	= "<div id=\"webfactor_modal_form-".$module->id."\" class=\"webfactor_modal_form webfactor_".$colorscheme."\">";//само окно
	$formrender .= "<span id=\"webfactor_modal_close-".$module->id."\" class=\"webfactor_modal_close\">x</span>";//кнопка ЗАКРЫТЬ
}else{
	$formrender	= "<div id=\"webfactor_static_form-".$module->id."\" class=\"webfactor_static_form webfactor_".$colorscheme."\">";
}

if($textbeforeform){
	$formrender 	.= "<div class=\"webfactor_predtext\">".str_replace(array("\r\n", "\r", "\n"), '',  $textbeforeform)."</div>";
}

$formrender 	.= "<form class=\"ajaxWebfactorForm\" id=\"ajax_webfactor_form".$module->id."\" data-modid = \"".$module->id."\">";

for($i=0; $i<count($field); $i++){
	$formrender .= $field[$i]['dataField'];
}

$formrender 	.= "<input type=\"hidden\" value=\"\" id=\"currentPage\"/>";

$formrender 	.= "<div class=\"clear20\"></div>";
$formrender 	.= "<input type=\"submit\" value=\"".$textsubmit."\" id=\"submit".$module->id."\" class=\"\"/>";
$formrender 	.= "</form>";
$formrender 	.= "<div class=\"webfactor_error-status".$module->id."\" style=\"display: none;\"></div>";
$formrender 	.= "<div class=\"webfactor_ok-status".$module->id."\" style=\"display: none;\"></div>";
$formrender 	.= "</div>";
if($onoffpopup): 
	$formrender .= "<div id=\"webfactor_overlay-".$module->id."\" class=\"webfactor_overlay\"></div>";//подложка
endif; ?>
<div class="webfactor_<?php echo $colorscheme; ?>">
<?php if($onoffpopup){?>
	
	<?php if($textbeforeCallButton){ ?>
	<div class="webfactor_predButton"><?php echo $textbeforeCallButton; ?></div>
	<?php } ?>
	<div data-id="webfactor_go-<?php echo $module->id; ?>" class="webfactor_go"><?php echo $textcallpopup;?></div>
<?php }else{
	echo $formrender;
 } ?>
</div>
<script>

(function($){
	$(document).ready(function(){	
<?php if($onoffpopup){ ?>

		$('body').append('<?php echo str_replace("'", "&#39;", $formrender); ?>');
		
		OpenModalForm(
			'[data-id=webfactor_go-<?php echo $module->id; ?>]',
			'#webfactor_modal_form-<?php echo $module->id; ?>',
			'#webfactor_overlay-<?php echo $module->id; ?>'
		);
		
		/* Закрытие модального окна, тут делаем то же самое но в обратном порядке */
		CloseModalForm(
			'#webfactor_modal_close-<?php echo $module->id; ?>, #webfactor_overlay-<?php echo $module->id; ?>',
			'#webfactor_overlay-<?php echo $module->id; ?>',
			'#webfactor_modal_form-<?php echo $module->id; ?>'
		);
	
<?php } ?>
});		
	
	$(document).on('click', 'input#submit<?php echo $module->id;?>', function () {
		var textSubmitButton = $('input#submit<?php echo $module->id; ?>').attr('value');
		<?php echo $jsbeforesend; ?>
		$( "#ajax_webfactor_form<?php echo $module->id; ?>" ).validate({
			submitHandler: function(form) {
				
				<?php //if(count($steArr) > 1){ ?>
				//var sendtoemailselect = $( 'select[name=sendtoemailselect<?php echo $module->id; ?>]' ).val();
				//alert(sendtoemailselect);
				<?php //} ?>
				$('#submit<?php echo $module->id; ?>').attr('value','<?php echo JText::_($textwhensending); ?>').css('text-transform','none');
				/*Собираем данные с полей формы*/
				<?php echo $dataField; ?>

				request = {
					'option'		: 'com_ajax',
					'module'		: 'ajax_webfactor_form',
					'data-modid'	: '<?php echo $module->id; ?>',
					'modtitle'		: '<?php echo $module->title; ?>',
					'currentPage'	: document.location.href,
					<?php echo $ajaxRequest; ?>
					'format'	: 'raw'
				}
				$.ajax({
					type   : 'POST',
					data   : request,
					success: function (response) {
						<?php echo $spinecode; ?>
						
						<?php if($onoffpopup): ?>
						
							var posAfterSend = positionAfterSend('#webfactor_modal_form-<?php echo $module->id; ?>');
							
							$('#webfactor_modal_form-<?php echo $module->id; ?>').animate({top: -posAfterSend}, 400, function(){
									$('#ajax_webfactor_form<?php echo $module->id; ?>, #webfactor_modal_form-<?php echo $module->id; ?> .webfactor_predtext').fadeOut("500",function(){
								
											$('.webfactor_ok-status<?php echo $module->id; ?>').fadeIn("500");
											
											<?php if($clearaftersend): ?>
											
											hideAndClearFormAfterSend(
												'#webfactor_modal_form-<?php echo $module->id; ?>',
												'#webfactor_overlay-<?php echo $module->id; ?>',
												'#webfactor_modal_form-<?php echo $module->id; ?> .webfactor_ok-status<?php echo $module->id; ?>',
												response,
												textSubmitButton
												);
											
											<?php else: ?>
											
											hideBlockFormAfterSend(
												'#webfactor_modal_form-<?php echo $module->id; ?>',
												'#webfactor_overlay-<?php echo $module->id; ?>',
												'#webfactor_modal_form-<?php echo $module->id; ?> .webfactor_ok-status<?php echo $module->id; ?>',
												response);
												
											<?php endif; ?>
										});
								});
						<?php else: ?>
							
							$('#ajax_webfactor_form<?php echo $module->id; ?>, #webfactor_static_form-<?php echo $module->id; ?> .webfactor_predtext').fadeOut("500",function(){
						
									$('.webfactor_ok-status<?php echo $module->id; ?>').fadeIn("500");
									
									<?php if($clearaftersend): ?>
									
									hideAndClearStaticFormAfterSend(
										'#webfactor_static_form-<?php echo $module->id; ?>',
										'#webfactor_static_form-<?php echo $module->id; ?> .webfactor_ok-status<?php echo $module->id; ?>',
										response,
										textSubmitButton);
									
									<?php else: ?>
									
									hideBlockStaticFormAfterSend(
										'#webfactor_static_form-<?php echo $module->id; ?>',
										'#webfactor_static_form-<?php echo $module->id; ?> .webfactor_ok-status<?php echo $module->id; ?>',
										response);
										
									<?php endif; ?>
								});
						<?php endif; ?>						
					}
				});
				return false;
			}
		});
	});
})(jQuery)
</script>